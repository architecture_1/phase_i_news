package com.nx.arch.transactionmsg.model;

/**
 * @类名称 State.java
 * @类描述 服务状态类
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月12日 下午4:15:15
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月12日             
 *     ----------------------------------------------
 * </pre>
 */
public enum State {
    /**
     * 服务创建态(非开始)
     */
    CREATE,
    /**
     * 服务运行态
     */
    RUNNING,
    /**
     * 服务关闭态
     */
    CLOSED,
    /**
     * 服务失败态
     */
    FAILED;
}
