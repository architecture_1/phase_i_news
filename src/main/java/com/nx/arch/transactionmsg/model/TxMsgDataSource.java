package com.nx.arch.transactionmsg.model;

/**
 * @类名称 TxMsgDataSource.java
 * @类描述 数据源信息对象
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月13日 下午8:35:14
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月13日             
 *     ----------------------------------------------
 * </pre>
 */
public class TxMsgDataSource {
    
    private String url;
    
    private String username;
    
    private String password;
    
    public TxMsgDataSource() {
        
    }
    
    /**
     * @param url db url需要和业务datasource中配置的url一模一样 
     * @param username
     * @param password
     */
    public TxMsgDataSource(String url, String username, String password) {
        super();
        this.url = url;
        this.username = username;
        this.password = password;
    }
    
    public String getUrl() {
        return url;
    }
    
    /**
     * 
     * @param url db url需要和业务datasource中配置的url一模一样 
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public String toString() {
        return "TxMsgDataSource [url=" + url + ", username=" + username + ", password=" + password + "]";
    }
    
}
