package com.nx.arch.transactionmsg;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadTest {
    public static final Logger log = LoggerFactory.getLogger(HelloTest.class);
    public static void main(String[] args){
        //ScheduledExecutorService 会把这个异常吞了，但是thread 不会
        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(2);
        ScheduledFuture<?> future = scheduledExecutor.scheduleAtFixedRate(new HelloRun(), 10,1000, TimeUnit.MILLISECONDS); 
        Thread thread = new Thread(new HelloRun(),"Hello_run_Thread");
        thread.start();
        try {
            System.out.println(future.get());
            Thread.sleep(1000000000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
    static class HelloRun implements Runnable {
        int i =0;
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("System.out." + System.currentTimeMillis());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
